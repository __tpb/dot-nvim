local M = {}

function M.setup()
	require("nvim-treesitter.configs").setup({
		ensure_installed = {
			"astro",
			"bash",
			"cmake",
			"c",
			"cpp",
			"commonlisp",
			"css",
			"html",
			"javascript",
			"json",
			"lua",
			"markdown",
			"make",
			"norg",
			"python",
			"regex",
			"rust",
			"scss",
			"sql",
			"svelte",
			"toml",
			"tsx",
			"typescript",
			"yaml",
			"vim",
		},
		highlight = {
			enable = true,
		},
	})
end

return M
