local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable",
		lazypath,
	})
end

vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	"folke/lazy.nvim",

	-- themes
	"bluz71/vim-moonfly-colors",
	"ofirgall/ofirkai.nvim",
	"rebelot/kanagawa.nvim",
	"sainnhe/gruvbox-material",
	"sainnhe/everforest",
	"savq/melange",

	"folke/zen-mode.nvim",
	"nvim-lualine/lualine.nvim",

	"kyazdani42/nvim-web-devicons",

	{
		"jose-elias-alvarez/null-ls.nvim",
		config = function()
			require("plugins.null_ls").setup()
		end,
	},

	{
		"neovim/nvim-lspconfig",
		config = function()
			require("plugins.lspconfig").setup()
		end,
	},

	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
		dependencies = {
			"nvim-treesitter/playground",
		},
		config = function()
			require("plugins.treesitter").setup()
		end,
	},

	{
		"hrsh7th/nvim-cmp",
		dependencies = {
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-nvim-lua",
			"hrsh7th/cmp-buffer",
			"hrsh7th/cmp-path",
			"hrsh7th/cmp-cmdline",
			{
				"L3MON4D3/LuaSnip",
				build = "make install_jsregexp",
			},
			"saadparwaiz1/cmp_luasnip",
			"onsails/lspkind.nvim",
		},
		config = function()
			require("plugins.cmp").setup()
		end,
	},

	{
		"numToStr/Comment.nvim",
		config = function()
			require("Comment").setup()
		end,
	},

	{
		"windwp/nvim-autopairs",
		config = function()
			require("nvim-autopairs").setup()
		end,
	},

	{
		"ggandor/leap.nvim",
		dependencies = {
			"ggandor/leap-spooky.nvim",
		},
		config = function()
			require("leap").add_default_mappings()
			require("leap-spooky").setup({
				affixes = {
					remote = { window = "r", cross_window = "R" },
					magnetic = { window = "m", cross_window = "M" },
				},
			})
		end,
	},

	{
		"toppair/peek.nvim",
		build = "deno task --quiet build:fast",
		config = function()
			require("peek").setup()
		end,
	},

	{
		"lukas-reineke/indent-blankline.nvim",
		config = function()
			require("indent_blankline").setup({
				show_current_context = true,
			})
		end,
	},

	{
		"nvim-telescope/telescope.nvim",
		dependencies = {
			"nvim-lua/plenary.nvim",
			{
				"nvim-telescope/telescope-fzf-native.nvim",
				build = "cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build",
			},
		},
		config = function()
			require("plugins.telescope").setup()
		end,
	},

	"tpope/vim-dadbod",
	"kristijanhusak/vim-dadbod-completion",
	"kristijanhusak/vim-dadbod-ui",

	--git
	{
		"lewis6991/gitsigns.nvim",
		config = function()
			require("gitsigns").setup()
		end,
	},
	{
		"sindrets/diffview.nvim",
		dependencies = {
			"nvim-lua/plenary.nvim",
		},
	},
	-- folke/todo-comments
	-- folke/noice
	-- neo-tree
	-- peek.nvim
	-- rust-tools.nvim
	-- symbols-outline.nvim
	-- palenightfall
	-- lazygit or toggleterm
	-- git-messenger
	-- nvim-spectre
})

local builtin = require("telescope.builtin")
--find
vim.keymap.set("n", "<leader>ff", builtin.find_files, {})
vim.keymap.set("n", "<leader>fr", builtin.oldfiles, {})
vim.keymap.set("n", "<leader>fb", builtin.buffers, {})
vim.keymap.set("n", "<leader>fh", builtin.help_tags, {})
vim.keymap.set("n", "<leader>fg", builtin.git_files, {})
--search
vim.keymap.set("n", "<leader>sb", builtin.current_buffer_fuzzy_find, {})
vim.keymap.set("n", "<leader>ss", builtin.current_buffer_fuzzy_find, {})
vim.keymap.set("n", "<leader>sg", builtin.live_grep, {})
vim.keymap.set("n", "<leader>sG", builtin.grep_string, {})
vim.keymap.set("n", "<leader>sc", builtin.quickfix, {})
--toggle
vim.keymap.set("n", "<leader>tt", builtin.colorscheme, {})
vim.keymap.set("n", "<leader>tp", "<cmd>Telescope commands<CR>", {})
vim.keymap.set("n", "<leader>tz", "<cmd>ZenMode<CR>", {})
--explorer
vim.keymap.set("n", "<leader>ee", "<cmd>Vexplore<CR>", {})
--code
vim.keymap.set("n", "<leader>cl", "<cmd>luafile %<CR>", {})
--lazy
vim.keymap.set("n", "<leader>ps", "<cmd>Lazy sync<CR>", {})
--buffer
vim.keymap.set("n", "<leader>bb", builtin.buffers, {})
vim.keymap.set("n", "<leader>bs", "<cmd>w<CR>", {})
vim.keymap.set("n", "<leader>bS", "<cmd>w!<CR>", {})
vim.keymap.set("n", "<leader>bd", "<cmd>bd<CR>", {})
vim.keymap.set("n", "<leader>bD", "<cmd>bd!<CR>", {})
vim.keymap.set("n", "<leader>bp", "<cmd>bp<CR>", {})
vim.keymap.set("n", "<leader>bn", "<cmd>bn<CR>", {})
vim.keymap.set("n", "<leader><leader>", "<C-^>", {})
--window
vim.keymap.set("n", "<leader>ws", "<C-w>s", {})
vim.keymap.set("n", "<leader>wv", "<C-w>v", {})
vim.keymap.set("n", "<leader>wo", "<C-w>o", {})
vim.keymap.set("n", "<leader>wq", "<C-w>q", {})
vim.keymap.set("n", "<leader>wh", "<C-w>h", {})
vim.keymap.set("n", "<leader>wl", "<C-w>l", {})
vim.keymap.set("n", "<leader>wH", "<C-w>H", {})
vim.keymap.set("n", "<leader>wL", "<C-w>L", {})
vim.keymap.set("n", "<leader>wj", "<C-w>j", {})
vim.keymap.set("n", "<leader>wk", "<C-w>k", {})
vim.keymap.set("n", "<leader>wJ", "<C-w>J", {})
vim.keymap.set("n", "<leader>wK", "<C-w>K", {})
--(c)uickfix
vim.keymap.set("n", "<leader>cq", "<cmd>cclo<CR>", {})
vim.keymap.set("n", "<leader>co", "<cmd>copen<CR>", {})
vim.keymap.set("n", "<leader>cp", "<cmd>colder<CR>", {})
vim.keymap.set("n", "<leader>cn", "<cmd>cnewer<CR>", {})
vim.keymap.set("n", "<leader>clq", "<cmd>close<CR>", {})
vim.keymap.set("n", "<leader>clo", "<cmd>lopen<CR>", {})
vim.keymap.set("n", "<leader>clp", "<cmd>lolder<CR>", {})
vim.keymap.set("n", "<leader>cln", "<cmd>lnewer<CR>", {})
--cd
vim.keymap.set("n", "<leader>hh", "<cmd>cd %:p:h<CR>", {})
--help
vim.keymap.set("n", "<leader>hc", "<cmd>e ~/.config/nvim/init.lua<CR>", {})

vim.cmd(
	[[autocmd FileType sql,mysql,plsql lua require('cmp').setup.buffer({ sources = {{ name = 'vim-dadbod-completion' }} })]]
)
