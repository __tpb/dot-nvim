local bin = vim.fn.stdpath("config") .. "\\bin\\"
local null_ls = require("null-ls")

local M = {}

function M.setup()
	local sources = {
		null_ls.builtins.diagnostics.markdownlint_cli2.with({
			prefer_local = "node_modules/.bin/",
			command = "markdownlint-cli2.cmd",
			-- args = { "$FILENAME" }
		}),
		null_ls.builtins.formatting.prettier.with({
			-- command = node .. 'prettierd.cmd'
			prefer_local = "node_modules/.bin/",
			command = "prettierd.cmd",
		}),
		null_ls.builtins.formatting.stylua.with({
			command = bin .. "stylua.exe",
		}),
	}

	null_ls.setup({ log_level = "debug", sources = sources })
end

return M
