local g = vim.g
local opt = vim.opt

g.mapleader = " "
opt.termguicolors = true
opt.cul = true
opt.cmdheight = 1
opt.laststatus = 3
opt.relativenumber = true
opt.number = true
opt.expandtab = true
opt.tabstop = 2
opt.softtabstop = 2
opt.smartindent = true
opt.shiftwidth = 2
opt.expandtab = true
opt.numberwidth = 2
opt.signcolumn = "yes"
opt.ignorecase = true
opt.smartcase = true
opt.ruler = false
opt.timeoutlen = 300
opt.updatetime = 250
g.nowrap = true

g.gruvbox_material_background = "hard"
g.everforest_background = "hard"

if vim.fn.executable("rg") then
	opt.grepprg = [[rg --no-heading --vimgrep]]
	opt.grepformat = [[%f:%l:%c:%m]]
end
