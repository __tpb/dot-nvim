if vim.fn.has("win64") then
	WINDOWS = true
else
	WINDOWS = false
end

if vim.fn.has("unix") then
	LINUX = true
else
	LINUX = false
end

if vim.g.vscode then
	VSCODE = true
else
	VSCODE = false
end
