if vim.loader then
  vim.loader.enable()
  vim.schedule(function()
    vim.notify("nvim cache is enabled")
  end)
end

require("core")

vim.opt.background = "dark"
vim.cmd.colorscheme("everforest")
require("lualine").setup({ options = { theme = "everforest" } })
